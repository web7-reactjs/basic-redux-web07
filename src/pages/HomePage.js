import React from 'react'
import { useSelector } from 'react-redux'

const HomePage = () => {
  let users =   useSelector(state=> state.users.users)
  return (
    <div>
        {
            users.map((user)=> 
            <h2>{user.id}</h2>
            )
        }
    </div>
  )
}

export default HomePage