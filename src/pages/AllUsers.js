import axios from "axios";
import React, { useState, useEffect } from "react";
import MyCard from "../components/MyCard";
import { useDispatch } from "react-redux";
import { GET_ALL_USERS } from "../service/userService";
import {userAdd} from "../redux/userSlice";
const AllUsers = () => {
  let dispatch = useDispatch();
  const [users, setUsers] = useState([]);

  useEffect(() => {
  GET_ALL_USERS().then((response )=> {
    console.log("All users : ", response)
    setUsers(response)
    dispatch(userAdd(response))
  }  
    )
   
  }, []);

//    dispatch(users)
  return (
    <div className="container">
      <div className="row">
        {users.map((user) => (
          <div className="col-4">
            <MyCard user={user} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default AllUsers;
