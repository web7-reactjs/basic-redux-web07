import React from 'react'
import { useSelector } from 'react-redux'

const AccountService = () => {
  const users = useSelector(state => state.users.users)
  console.log("users : ", users)
  return (
    <div> 

        {
            users.map((user)=>
            <h1> {user.email} </h1>
            )
        }
    </div>
  )
}

export default AccountService