
import './App.css';
import MyNavBar from './components/MyNavBar';
import MyCard from './components/MyCard'
import 'bootstrap/dist/css/bootstrap.min.css'
import AllUsers from './pages/AllUsers';
import { BrowserRouter as BigRouter, Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import AccountService from './pages/AccountService';
function App() {

 
  return (
    <div  >
      <BigRouter>
        <MyNavBar/>
        <Routes>
           <Route path='/' index element={<AllUsers/>}/>
           <Route path='homepage' element={<HomePage/>}/>
           <Route path='account' element={<AccountService/>}/>
        </Routes>
      </BigRouter>
        
        
       
    </div>
  );
}

export default App;
