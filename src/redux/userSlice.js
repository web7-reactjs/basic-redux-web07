import { createSlice } from "@reduxjs/toolkit";


const initialState = {
    users:[]
}

const userSlice = createSlice({
    name : "users", 
    initialState, 
    reducers:{
        userAdd: (state, action )=>{
            state.users = action.payload
        }
    }
})
export const { userAdd } = userSlice.actions

export default userSlice.reducer