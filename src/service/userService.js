import axios from "axios"


export const GET_ALL_USERS = async()=>{
    let response  = await axios.get("https://api.escuelajs.co/api/v1/users")

    return response.data 
}