import { configureStore } from "@reduxjs/toolkit";
// import todoReducer from '../features/todo/todoSlice';
import userReducer from "../redux/userSlice";
export default configureStore({
  reducer: {
    users: userReducer ,
  },
});